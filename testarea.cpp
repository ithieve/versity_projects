//

//preprocessor directives
#include <iostream>
//#inlcude <fstream> //fileoutput
//#include <sstream> //to convert string to num
#include <conio.h> //getch
//#include <vector>  //vectors
#include <cstdlib> //basic c functions
#include <string>
//#include <cmath>   //c maths functions
#include <iomanip> // manipulators
//#include <cstdio>  //almost same as cstdlib
//#include <ctime>   //time input required as seed for rng
//#include <algorithm>

using namespace std;

string gradeRecieved(int n){
	if(n==100){
		return "A**";
	}
	else if(n<100 && n>=90){
		return "A*";
	}
	else if(n<90 && n>=80){
		return "A";
	}
	else if(n<80 && n>=70){
		return "B";
	}
	else if(n<70 && n>=60){
		return "C";
	}
	else if(n<60 && n>=50){
		return "D";
	}
	else if(n<50 && n>=40){
		return "E";
	}
	else if(n<40){
		return "F";
	}
}

int main(){

	const int subMax=100;
	const int holders=3; //score and name
	//Initialization
	int subNum=0;
	int total=0;
	int average=0;
	
	string grades[subMax][holders];
	string hold;
	int h=0;
	//Get number of subjects
		system("cls");
	//title menu
	cout
	<<"============================================================"<<endl
	<<"=          Welcome to school grades calculator             ="<<endl
	<<"= Please Input the number for the specific program to use. ="<<endl
	<<"============================================================"<<endl
	<<endl
	<<"Please insert the number of Subjects:";
	cin>>subNum;
	
	system("cls");
	cout
	<<"You have selected "<<subNum<<" subjects."<<endl
	<<"========================================"<<endl
	<<endl
	<<"Please Insert the name and score of the subjects in the following format"<<endl
	<<"[ Subject_name subject_score ]"<<endl
	<<endl;
		for (int i=0; i<subNum; ++i){
			cin>>grades[i][1]>>grades[i][2];
			hold=grades[i][2];
			h=stoi(hold);
			grades[i][3]=gradeRecieved(h);
			total +=h;
			
		}
	system("cls");
	
	cout
	<<"\t\tResults"<<endl
	<<"========================="<<endl
	<<endl
	<<"Number\t|\tSubject Name\t|\tScore\t|\tGrade"<<endl
	<<"================================================================================"<<endl<<endl;	
	
	for (int i=0; i<subNum; ++i){
		cout<<"\t"<<i+1<<setw(18)<<grades[i][1]<<setw(18)<<grades[i][2]<<setw(18)<<grades[i][3]<<endl;
		
	}
	cout<<endl<<endl;
	cout<<"Total : "<<total<<" out of "<<subNum*100<<endl;
	cout<<"Average : "<<(float)total/(float)subNum<<" out of 100"<<endl;
	cout<<"Overall CGPA : "<<(((float)total/(float)subNum)/100.0)*(4.0)<<endl;
	cout<<"Overall Grade: "<<gradeRecieved(total/subNum)<<endl;
				

	
	
	
	
return 0;
}

