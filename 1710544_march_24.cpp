//IUB CSE101 Section 1 Spring 2018
//C++ Project V4 (road to submission 2)
//===================================
//This program contains several different operations organized in menus

#include <iostream>//C++ input output stream
#include <cmath>   //The Cmath library
#include <cstdlib> //This file also includes C standard library functions
#include <conio.h> //Old Console IO functions
#include <string> //String manipulation functions
#include <iomanip> //IO manipulators


using namespace std;

	//Functions Prototypes//
//Print Functions
void printCircle(float radius); 	//Prints a circle with radius
void printRect(float l, float b);	//Prints a rectangle with sides
void printTriangle(float base, float height); //Prints a triangle with base and height
void printSphere(float radius);	 //Prints a sphere with radius
void printCube(float length, float base, float height); //Prints a cube with length base and height

//Menu Printing 
void menu_main(void);  			//main menu
void menu_maths(void); 			//Maths menu
void menu_mathShapes(void); 	//Maths::shapes menu
void menu_mathGrades(void); 	//Maths::grades calculator
void menu_extras(void);			//Extra utilities


//Accesories Function
void pressToContinue(void); //Simple "Press any key to continue"
bool doAgain(void); //Asks user for repeating the task
int mainLoop(void); //The main execution point of the program
					////Clears command line buffers
void cliobuf(void){
	cin.clear();
	fflush(stdin);	
}
//Added for Submission 2
string gradeRecieved(int n){
	if(n==100){
		return "A**";
	}
	else if(n<100 && n>=90){
		return "A*";
	}
	else if(n<90 && n>=80){
		return "A";
	}
	else if(n<80 && n>=70){
		return "B";
	}
	else if(n<70 && n>=60){
		return "C";
	}
	else if(n<60 && n>=50){
		return "D";
	}
	else if(n<50 && n>=40){
		return "E";
	}
	else if(n<40){
		return "F";
	}
}


//Shapes calculations Functions
int areaCircle(void);
int areaRect(void);
int areaTrig(void);
int volSphere(void);
int volCylinder(void);
int volCube(void);


//Global Constants
	const float pi=3.14159265358979323;


	/**Main Function**/
/*================================*/
int main(){


	//Constants
	/**Program Loop Starts Here**/
	mainLoop();

}

//**End of MAIN**//

    /**Functions**/
// Shapes Printing Functions
void printCircle(float radius){

	cout
	<<"    =  ="<<endl
	<<" =        ="<<endl
	<<"=     .____="<<"\tr= "<<radius<<endl
	<<"=        r ="<<endl
	<<" =        ="<<endl
	<<"    =  ="<<endl;

}


void printRect(float l, float b){
	cout
	<<"============"<<endl
	<<"+          +"<<endl
	<<"+          + l="<<l<<endl
	<<"+          +"<<endl
	<<"============"<<endl
	<<"    b="<<b<<endl;
}

void printTriangle(float base, float height){
    cout
		<<"    .      --"<<endl
		<<"   /+\\     |"<<endl
		<<"  / + \\    |"<<endl
		<<" /  +  \\   |height= "<<height<<endl
		<<"/   +   \\  |"<<endl
		<<"========= --"<<endl
		<<"  base= "<<base<<endl;


}

void printSphere(float radius){
	cout
		<<"  .adAHHHAbn."<<endl
		<<" dHHHHHHHHHHHb "<<endl
		<<"dHHHHHHHHHHHHHb"<<endl
		<<"HHHHHHHHHHHHHHH"<<endl
		<<"VHHHHHHHHHHHHHP"<<endl
		<<" YHHHHHHHHHHHP"<<endl
		<<"  \"^YUHHHUP^\""<<endl
		<<"      \"~\""<<endl
		<<"      |.......|"<<endl
		<<"      Radius="<<radius<<endl;

}

void printCylinder(float radius, float height){
    cout
        <<" .------.  --"<<endl
        <<"(    ._r_)  |"<<endl
        <<"|~------~|  |"<<endl
        <<"|        |  |"<<endl
        <<"|        |  | height = "<<height<<endl
        <<"|        |  |"<<endl
        <<"|        |  |"<<endl
        <<" \"------\"  --"<<endl
        <<"    radius = "<<radius<<endl;

}

void printCube(float length, float base, float height){
    cout
        <<"   e-------f"<<endl
        <<"  /|      /|"<<endl
        <<" / |     / | Height = "<<height<<endl
        <<"a--|----b  |"<<endl
        <<"|  g----|--h"<<endl
        <<"| /     | /  length = "<<length<<endl
        <<"c-------d"<<endl
        <<"  Base = "<<base<<endl;
}


 /******************MENU FUNCTIONS******************************/

//Main-Menu
//lvl0
void menu_main(void){
	system("cls");
	//title menu
	cout
	<<"============================================================"<<endl
	<<"= This program can perform several types of Functions.	  ="<<endl
	<<"= Please Input the number for the specific program to use. ="<<endl
	<<"============================================================"<<endl
	<<endl
	<<"Programs available:"<<endl
	<<"\t1. Maths"<<endl
	<<endl
	<<"Enter -1 to exit.."
	<<endl
	<<"Please make a selection: ";
	int menu=0;
	cin>>menu;
	switch(menu){
		case 1:
			cliobuf();
            menu_maths();
            menu_main();
            
		case -1:
			cliobuf();
			system("cls");
			cout<<"The Program will exit now"<<endl;
			pressToContinue();
			exit(0);
			break;
		case 0:
			//GO back 1 function (not necessary here)
		default:
			cout<<"Invalid menu selection, Please try again!!"<<endl;
			cliobuf();
			pressToContinue();
			cliobuf();
			menu_main();
	}

	
}
//Menu::maths
//LV0-1
void menu_maths(void){
	system("cls");
	//title menu
	cout
	<<"============================================================"<<endl
	<<"= This program can perform several types of calculations.  ="<<endl
	<<"= Please Input the number for the specific program to use. ="<<endl
	<<"============================================================"<<endl
	<<endl
	<<"Programs available:"<<endl
	<<"\t1. Area and volumes of Shapes"<<endl
	<<"\t2. Grade Calculator"
	<<endl
	<<"Enter -1 to exit or 0 to go back one level.."
	<<endl
	<<"Please make a selection: ";
	
	int menu=0;
	cin>>menu;
	switch(menu){
		case 1:
			cliobuf();
            menu_mathShapes();
            menu_maths();
        case 2:
        	cliobuf();
        	menu_mathGrades();
        	menu_maths();
		case -1:
			cliobuf();
			system("cls");
			cout<<"The Program will exit now"<<endl;
			pressToContinue();
			exit(0);
			break;
		case 0:
			cliobuf();
			menu_main(); //Go back one function
		default:
			cout<<"Invalid menu selection, Please try again!!"<<endl;
			cliobuf();
			pressToContinue();
			cliobuf();
			menu_maths(); //Do a repeat of this function
	}
}


//Menu::Extras
//LV0-last
void menu_extras(void){
	system("cls");
	//title menu
	cout
	<<"============================================================"<<endl
	<<"= This program can perform several types of calculations.  ="<<endl
	<<"= Please Input the number for the specific program to use. ="<<endl
	<<"============================================================"<<endl
	<<endl
	<<"Programs available:"<<endl
	<<"\t1. Area and volumes of Shapes"<<endl
	<<"\t2. Grade Calculator"
	<<endl
	<<"Enter -1 to exit or 0 to go back one level.."
	<<endl
	<<"Please make a selection: ";
	
	int menu=0;
	cin>>menu;
	switch(menu){
		case 1:
			cliobuf();
            menu_mathShapes();
            menu_maths();
        case 2:
        	cliobuf();
        	menu_mathGrades();
        	menu_maths();
		case -1:
			cliobuf();
			system("cls");
			cout<<"The Program will exit now"<<endl;
			pressToContinue();
			exit(0);
			break;
		case 0:
			cliobuf();
			menu_main(); //Go back one function
		default:
			cout<<"Invalid menu selection, Please try again!!"<<endl;
			cliobuf();
			pressToContinue();
			cliobuf();
			menu_maths(); //Do a repeat of this function
	}
}





//Maths::Shapes
//Lvl0-1-1-1
void menu_mathShapes(void){
	system("cls");
	//title menu
	cout
	<<"============================================================"<<endl
	<<"= This program can perform several types of calculations.  ="<<endl
	<<"= Please Input the number for the specific program to use. ="<<endl
	<<"============================================================"<<endl
	<<endl
	<<"Programs available:"<<endl
	<<"\t1. Area of circle"<<endl
	<<"\t2. Area of rectangle"<<endl
	<<"\t3. Area of triangle"<<endl
	<<"\t4. Volume of cylinder"<<endl
	<<"\t5. Volume of sphere"<<endl
	<<"\t6. Volume of cube"<<endl
	<<endl
	<<"Enter -1 to exit or 0 to go back one level.."
	<<endl
	<<"Please make a selection: ";
	
	int menu=0;
	cin>>menu;
	switch(menu){
		case 1:
			cliobuf();
		    areaCircle();
		    cliobuf();
            menu_mathShapes();
        case 2:
        	cliobuf();
            areaRect();
            cliobuf();
            menu_mathShapes();
        case 3:
        	cliobuf();
            areaTrig();
            cliobuf();
            menu_mathShapes();
        case 4:
        	cliobuf();
            volCylinder();
            cliobuf();
            menu_mathShapes();
        case 5:
        	cliobuf();
            volSphere();
            menu_mathShapes();
        case 6:
        	cliobuf();
            volCube();
            menu_mathShapes();

		case -1:
			system("cls");
			cout<<"The Program will exit now"<<endl;
			pressToContinue();
			exit(0);
			break;
		case 0:
			cliobuf();
			menu_maths(); 
		default:
			cout<<"Invalid menu selection, Please try again!!"<<endl;
			cliobuf();
			pressToContinue();
			cliobuf();
			menu_mathShapes();
	}
}

//Maths::Grades
//Lvl0-1-1-2
//Submission 2
void menu_mathGrades(void){
	const int subMax=100;
	const int holders=3; //score and name
	//Initialization
	int subNum=0;
	int total=0;
	string grades[subMax][holders];
	string hold;
	int h=0;
	//Get number of subjects
		system("cls");
	//title menu
	cout
	<<"============================================================"<<endl
	<<"=          Welcome to school grades calculator             ="<<endl
	<<"= Please Input the number for the specific program to use. ="<<endl
	<<"============================================================"<<endl
	<<endl
	
	<<"Please insert the number of Subjects (Or enter -1 to go back): ";
	
	cin>>subNum;
	
	if(subNum==-1){
		cliobuf();
		menu_maths();
	}
	else if(subNum<0){
		cout<<"\nPlease try again"<<endl;
		pressToContinue();
		menu_mathGrades();
	}
	else{
		system("cls");
		cout
		<<"You have selected "<<subNum<<" subjects."<<endl
		<<"========================================"<<endl
		<<endl
		<<"Please Insert the name and score of the subjects in the following format"<<endl
		<<"[ Subject_name subject_score ]"<<endl
		<<"For example: Maths 65"<<endl
		<<endl;
			for (int i=0; i<subNum; ++i){
				cliobuf();
				cin>>grades[i][1]>>grades[i][2];
				hold=grades[i][2];
				h=stoi(hold);
				grades[i][3]=gradeRecieved(h);
				total +=h;
				
			}
		system("cls");
		
		cout
		<<"\t\tResults"<<endl
		<<"========================="<<endl
		<<endl
		<<"Number\t|\tSubject Name\t|\tScore\t|\tGrade"<<endl
		<<"================================================================================"<<endl<<endl;	
		
		for (int i=0; i<subNum; ++i){
			cout<<"\t"<<i+1<<setw(18)<<grades[i][1]<<setw(18)<<grades[i][2]<<setw(14)<<grades[i][3]<<endl;
			
		}
		cout<<endl<<endl;
		cout<<"Total : "<<total<<" out of "<<subNum*100<<endl;
		cout<<"Average : "<<(float)total/(float)subNum<<" out of 100"<<endl;
		cout<<"Overall CGPA : "<<(((float)total/(float)subNum)/100.0)*(4.0)<<endl;
		cout<<"Overall Grade: "<<gradeRecieved(total/subNum)<<endl;
		
		pressToContinue();
		if(doAgain()){
			cliobuf();
			menu_mathGrades();

		}
	}
	
	
	
	

}

/*************Accesories Functions***************/

//Press To continue
void pressToContinue(void){
	char c;
	cout<<"Please ENTER to continue...";
	c=getch();
	if(c == 0 || c == 224){ getch();
	}
}

//asks to do the same task again. Returns bool
bool doAgain(void){
	char c;
		system("cls");
		cout<<"Do you want to perform this task again?"<<endl;
		cout<<"Press \'Y\' for yes... : ";cin>>c;
		if(c=='Y' || c=='y'|| c=='1'){
			return true;
		}
		else{
			return false;
		}

}

//Calculation functions
int areaCircle(void){
		float radius;

		system("cls");
		cout<<"This program can calculate the area of a circle."<<endl
			<<"==============================================="<<endl
			<<"Please input the radius of the circle: "; cin>>radius;


		printCircle(radius);
		cout<<"===================================="<<endl;
		cout<<"\n\n The area of the circle is: "<<pi*pow(radius, 2)<<endl;
		cout<<endl<<endl;
		cliobuf();
		pressToContinue();
		cliobuf();
		if(doAgain()){
			cliobuf();
			areaCircle();
		}


}

int areaRect(void){
    float length=0;
    float base=0;
    system("cls");
    cout<<"This program can calculate the area of a Rectangle."<<endl
        <<"=================================================="<<endl
        <<"Please input the Base of the Rectangle: "; cin>>base;

    system("cls");
    printRect(length, base);
    cout<<"Please input the Length of the Rectangle: "; cin>>length;
    system("cls");
    printRect(length, base);
    cout<<"\n\nThe area of the rectangle is: "<<base*length<<endl
    <<endl;
    cliobuf();
    pressToContinue();
    cliobuf();
    if(doAgain()){
    	cliobuf();
        areaRect();
    }


}

int areaTrig(void){
    float height=0;
    float base=0;
    system("cls");
    cout<<"This program can calculate the area of a Triangle."<<endl
        <<"=================================================="<<endl
        <<"Please input the Base of the Triangle: "; cin>>base;
    system("cls");
    printTriangle(base, height);
    cout<<"Please input the Height of the Rectangle: "; cin>>height;
    system("cls");
    printTriangle(base, height);
    cout<<"\n\nThe area of the triangle is: "<<0.5*base*height<<endl
        <<endl;
    cliobuf();
    pressToContinue();
    cliobuf();
    if(doAgain()){
    	cliobuf();
        areaTrig();
    }

}

int volCylinder(void){
    	float radius=0;
		float height=0;
		system("cls");
		cout<<"This program can calculate the volume of a cylinder."<<endl
			<<"=================================================="<<endl;
        printCylinder(radius, height);
            cout<<endl
                <<"Please input the radius of the cylinder: "; cin>>radius;

		system("cls");
        cout<<"This program can calculate the volume of a cylinder."<<endl
            <<"=================================================="<<endl;
        printCylinder(radius, height);
		cout<<endl
            <<"Please input the height of the cylinder: "; cin>>height;
		system("cls");
		cout<<"This program can calculate the volume of a cylinder."<<endl
            <<"=================================================="<<endl;
		printCylinder(radius, height);
		cout<<"\n\nThe volume of the cylinder is: "<<pi * pow(radius, 2) * height<<endl
		<<endl;
		cliobuf();
		pressToContinue();
		cliobuf();
		if(doAgain()){
			cliobuf();
			volCylinder();
		}
}

int volSphere(void){
    float radius=0;
    system("cls");
    cout<<"This program can calculate the Volume of a Sphere."<<endl
        <<"=================================================="<<endl
        <<"Please input the Radius of the sphere: "; cin>>radius;
    cout<<endl;
    printSphere(radius);
    cout<<"\n---------------------------------"<<endl;
    cout<<"The volume of the sphere is: "<<((4.0/3.0)*pi*pow(radius, 3))<<endl<<endl;
    cout<<"==================================="<<endl;
    cliobuf();
	pressToContinue();
    cliobuf();
    if(doAgain()){
    	cliobuf();
        volSphere();
    }
}

int volCube(void){
    float length=0;
    float base=0;
    float height=0;
    system("cls");
    cout<<"This program can calculate the volume of a cube."<<endl
        <<"=================================================="<<endl;
    printCube(length, base, height);
    cout<<endl
        <<"Please input the length of the cube: "; cin>>length;
    system("cls");
    cout<<"This program can calculate the volume of a cube."<<endl
        <<"=================================================="<<endl;
    printCube(length, base, height);
    cout<<endl
        <<"Please input the height of the cube: "; cin>>height;
    system("cls");
    cout<<"This program can calculate the volume of a cube."<<endl
        <<"=================================================="<<endl;
    printCube(length, base, height);
    cout<<endl
        <<"Please input the base of the cube: "; cin>>base;
    system("cls");
    cout<<"This program can calculate the volume of a cube."<<endl
        <<"=================================================="<<endl;
    printCube(length, base, height);

    cout<<"\n\nThe volume of the cube is: "<<length * base * height<<endl
        <<endl;
	cliobuf();
    pressToContinue();
    cliobuf();

    if(doAgain()){
    	cliobuf();
		volCube();
        
		}

}






//Main loop Function

int mainLoop(){

	menu_main();

}

/**END OF SOURCE**/




/*LOGS*/
/*
Version 01:
-Bring down the project from the old
Version 02:
-Convert all operations to Functions
-set Up an exit methode (for mainLoop)
-Done: areacircle, areaRect, areaTrig, volCylinder, volSphere, volCube
-Submitted
Version 03:
-BUGFIX:: Function menu return
Version 04:
-Merge menu printing functions to menu operation functions
-Created a main menu function
-Seperate maths functions
-create Command Line IO Buffer cleaner
-Added School Grade Result calculator 
-Prepared for Submission 2

******Submission 2: Proj_1V4.cpp >> 1710544_March_17.cpp



*/

/*TODO*/
/*
-check if user inputs valid number in calculations

*/

/* --OBSOLETE CODE--


*/


